FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=kanboard/plugin-oauth2 versioning=semver extractVersion=^v(?<version>.+)$
ARG OAUTH2PLUGIN_VERSION=1.0.2

# renovate: datasource=github-releases depName=kanboard/kanboard versioning=semver extractVersion=^v(?<version>.+)$
ARG KANBOARD_VERSION=1.2.43
RUN curl -L https://github.com/kanboard/kanboard/tarball/v${KANBOARD_VERSION} | tar -xz --strip-components 1 -f - && \
    rm -rf /app/code/data && ln -s /app/data /app/code/data && \
    ln -s /run/kanboard/config.php /app/code/config.php

RUN mkdir -p /app/code/plugins/OAuth2 && \
    curl -L https://github.com/kanboard/plugin-oauth2/archive/refs/tags/v${OAUTH2PLUGIN_VERSION}.tar.gz | tar -xz --strip-components 1 -f - -C /app/code/plugins/OAuth2

RUN mv /app/code/plugins /app/code/plugins.orig && \
    ln -s /app/data/plugins /app/code/plugins && \
    chown -R www-data.www-data /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/kanboard.conf /etc/apache2/sites-enabled/kanboard.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite
# configure php. starting 1.2.0 kanboard uses the db to store sessions
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/kanboard/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

COPY config.php.template customconfig.php.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
