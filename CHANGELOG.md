[0.1.0]
* Initial version

[0.2.0]
* Improved apache configuration

[0.3.0]
* Set targetBoxVersion

[0.4.0]
* Update to version 1.0.25

[0.4.1]
* Update to version 1.0.26

[0.5.0]
* Add support for plugins
* Setup application URL correctly (for notifications)

[0.5.1]
* Add description on how to install plugins

[0.5.2]
* Update to version 1.0.27
* Allow login via username or email

[0.5.3]
* Use uid instead of username

[0.5.4]
* Revert uid change

[0.5.5]
* Update to version 1.0.28
* Use latest SMTP settings

[0.6.0]
* Update Kanboard to version 1.0.31

[0.7.0]
* REMEMBER TO UPDATE ANY PLUGINS
* Update Kanboard to version 1.0.32

[0.7.1]
* Update Kanboard to version 1.0.33

[0.8.0]
* Update Kanboard to version 1.0.34

[0.9.0]
* Update Kanboard to version 1.0.39
* Support Optional SSO install

[0.10.0]
* Update Kanboard to version 1.0.41
* Fix post install message
* Fix data location

[0.10.1]
* Fix description

[0.10.2]
* Fix screenshots

[0.11.0]
* Update Kanboard to version 1.0.42

[0.11.1]
* Bump memory limits for file upload to 64M

[0.11.2]
* Update to version 1.0.43

[0.12.0]
* Update to version 1.0.44
* (breaking) The calendar plugin must be updated

[0.13.0]
* Update to version 1.0.45

[0.13.1]
* Make sure application URL is setup correctly in non-sso mode

[1.0.0]
* Update to version 1.0.46
* Security Issues - Fix two privilege escalation issues: an authenticated standard user could reset the password of another user by altering form data.
* Add "Create another link" checkbox for internal link as in sub-task creation
* Updated translations
* Fix parsing issue in phpToBytes() method

[1.1.0]
* Update to version 1.0.47
* New features: Vietnamese translation
* Improvements: Updated translations
* Security Issues: Avoid people to alter other project resources by changing form data

[1.2.0]
* Update to version 1.0.48
* Add bulk subtasks creation
* Add filter by score/complexity
* Improved display of the header bar
* Displays bullets from lists in tooltips
* Updated translations
* Add tags and priority to task export
* Make the number of events stored in project activities configurable
* Do not use jQuery tooltip for task title in collapsed mode
* Remove dependency on Yarn
* Improve external task integration
* Add support for array parameters in automatic actions
* Add tooltip to subtask icons
* Add attribute title to external links
* Render a link if the reference is a URL
* Add icon to edit a task quickly on the board

[1.3.0]
* Update kanboard to version 1.1.0
* Remove feature "Allow everybody to access to this project" (You must define project members and groups)
* Add predefined templates for task descriptions
* Add the possibility to send tasks and comments to multiple recipients
* Add users, groups and projects search
* Add command line argument to display Kanboard version
* Add user backend provider system (to be used by external plugins)
* Add Romanian and Chinese (Taiwan) translation
* Task CSV import is now able to handle the priority, start date, tags and one external link
* Improve iCalendar feed to include tasks with start/end date and due date with a time
* Check if the start date is before due date

[1.4.0]
* Update kanboard to 1.1.1
* Task limit apply across all swimlanes
* Kanboard is now using the domain kanboard.org
* New automatic action to create a subtask assigned to the creator and start the timer
* New automatic action to stop the timer of subtasks
* Add command line tool to disable projects not touched during one year
* Add config option to exclude fields from auth providers sync
* Add new plugin hooks
* Open audio files in a new tab
* Disable private projects when disabling a user
* Allow administrators to update username of remote users
* Improve layout on mobile/tablet devices
* Changed board column headings to show swimlane-column total in bold
* Enable dragging to collapsed columns
* Add missing checks for requirements

[1.5.0]
* Update kanboard to version 1.2.0
* PHP sessions are now stored into the database, in this way, it's easier to run Kanboard behind a load-balancer
* Copy category from parent task when creating a task from a subtask
* Translation updates and improvements

[1.5.1]
* Update kanboard to version 1.2.1
* Add automatic action to change column once a start date is reached
* Add automatic action to change color once start date is reached
* Add CSS class to categories to allow custom styling
* Add option to disable Mysql SSL server verification
* Add timeout parameter for database connection
* Add error log for authentication failure to allow fail2ban integration
* Set the correct swimlane/column ID when moving a task via its internal dialog
* Allow filtering for tasks without due date
* Add plugin hook 'aftersave' after creating Task
* Run SessionHandler::write() into a transaction
* Remove dependency on PicoFeed
* Add CSRF check for task and project files upload
* Add missing CSRF check on avatar upload form
* Add missing CSRF check in saveUploadDB() method
* Update Vagrantfile to use Ubuntu Xenial
* Send event author in webhook notification

[1.5.2]
* Update kanboard to version 1.2.2
* Add thumbnail quality parameter (default to 95)
* Add author name and email arguments to mail client
* Update translations
* Fix broken daily summary export
* Fix role precedence in LDAP integration

[1.5.3]
* Update Kanboard to version 1.2.3
* Add Project MetaData API calls
* Add default filter per user
* Use utf8mb4 encoding for MySQL instead of utf8 (Emoji support)
* Increase text fields length in several tables
* Move documentation to https://docs.kanboard.org/
* Make sure no empty group is submitted on project permissions page
* Translate subtasks status and internal links labels in notifications
* Add link to tasks and projects in overdue notifications
* Add missing translations
* Move custom libs to the source tree
* Fix margin for task recurrence tooltip

[1.5.4]
* Update Kanboard to 1.2.4
* Rewrite tooltip code without jQuery
* Update Parsedown library
* Remove all attachments when removing a project
* Improve whitespace handling in "cli locale:compare" command
* Don't markdown project owner's name in header tooltip
* Make hardcoded hours string translatable
* Translation updates

[1.5.5]
* Update Kanboard to 1.2.5

[1.6.0]
* Use latest base image

[1.6.1]
* Update Kanboard to 1.2.6
* Escape table name 'groups' because groups is a reserved word as of MySql 8.0.2
* Reduce number of SQL queries when doing groups sync
* Make swimlane filter compatible with numeric title
* Duplicate reference fields when duplicating a task
* Do not try to redirect to login page when offline
* Define fixed width for auto-complete dropdown
* Fix task drag and drop slowdown when a column is hidden
* Make PLUGINS_DIR absolute in config.default.php
* Update translations and fix typos

[1.6.2]
* Fix installation of plugins

[1.6.3]
* Update Kanboard to 1.2.7

[1.7.0]
* Update Kanboard to 1.2.8
* Limit avatar image size
* Avoid CSRF in users CSV import
* Avoid XSS in pagination sorting
* Do not show projects dropdown when prompting the 2FA code
* Always returns a 404 instead of 403 to avoid people discovering users
* Check if user role has changed while the session is open
* Add missing CSRF check in TwoFactorController::deactivate()
* Hide edit button when user cannot edit task
* Fix permission check before "Assign to me"
* Fix permission check before showing project options
* Fix assignable users on a group with a custom role
* Fix import of automatic actions when parameters are "unassigned" or "no category"
* Limit assignee drop-down selector scope

[1.8.0]
* Update Kanboard to 1.2.9
* Add Slovak translation
* Update translations
* Changes search by reference to case insentive
* Add missing webhook event: task.move.project
* Add new actions to reorder tasks by column

[1.9.0]
* Remove admin propagation

[1.10.0]
* Use manifest v2

[1.10.1]
* Update Kanboard to 1.2.10

[1.10.2]
* Run db migrations manually to prevent db migration race

[1.10.3]
* Update Kanboard to 1.2.11

[1.10.4]
* Update Kanboard to 1.2.12

[1.10.5]
* Update Kanboard to 1.2.13

[1.11.0]
* Update Kanboard to 1.2.14
* Update translations
* Add new event subtask.create_update
* Replace Travis CI by GitHub Actions
* Add option to enable or disable global tags per projects
* Show group membership(s) in user summary and user list
* Docker: use real hostname instead of "localhost"
* Add new task/project image hooks
* Fix invalid RSS feed encoding
* Add new plugin hooks
* Rename "private" projects to "personal"
* Add per-project and per-swimlane task limits
* Use parent task color when converting a subtask to task

[1.12.0]
* Use latest base image 2.0.0

[1.12.1]
* Update Kanboard to 1.2.15
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.15)
* Added PUT method using CURLOPT_CUSTOMREQUEST
* Open large modal when clicking on edit category link
* Set margin-bottom at 0 only for the last child of a tooltip element
* Prevent last swimlane to be hidden if there is only one
* Make tooltip events bubble
* Keep newlines in markdown
* Show the color dropdown when creating a new automatic action
* Update translations
* Add action to assign a user when the swimlane change

[1.13.0]
* Make php.ini customizable
* Fix screenshot links
* Add forum url to manifest

[1.13.1]
* Update Kanbaord to 1.2.16
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.16)
* Update Composer dependencies
* Update translations
* Add link to toggle column scrolling in board view
* Add missing environment variables in php-fpm config
* Add setting that makes possible any new LDAP user to be Manager by default
* Add ARIA label to modal link with title attribute
* Add ARIA label to user mention
* Add ARIA label to letter avatars
* Add ARIA label to project select role without label
* Add ARIA label to dropdown autocomplete without label
* Add ARIA label to form text editor without label
* Add ARIA label to icons with title attributes
* Add ARIA label for form inputs without labels
* Add ARIA label for elements with titles
* Add hidden accessible form input labels
* Add hidden accessible titles
* Hide user name from screen readers
* Correct table collapsed column titles
* Prevent the original page from being modified by the opened link
* Allow email to be retrieve by SSO ReverseProxy
* Fix grammatically incorrect error message
* Add option to configure SMTP HELO name
* Add new config parameter SESSION_HANDLER
* Fix clearing of all Javascript storage
* Added standard notification footer to comment email template

[1.13.2]
* Do not sync user's role from LDAP

[1.13.3]
* Update Kanboard to 1.2.17
* Fix grammatical errors
* Add autocomplete attribute to HTML forms
* Added "Mexican Peso" to the list of currencies
* Added an option to send a copy of all generated e-mails to a BCC address
* Don't force role of users if no LDAP groups defined
* Keep the tags when converting a subtask to task
* Bump symfony/stopwatch from 5.1.8 to 5.2.0
* Bump pimple/pimple from 3.3.0 to 3.3.1
* Bump symfony/stopwatch from 5.2.0 to 5.2.1
* Publish Docker images to GitHub container registry in addition to Docker Hub
* Use Github Actions to publish Docker images
* Check if the user is assigned to any role in the project
* Fix tasks.swimlane_id foreign key for Sqlite
* Remove unused namespaces
* Add mk_MK (Macedonian) translation
* Update translations

[1.13.4]
* Update Kanbaord to 1.2.18
* [Changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.18)
* Add missing pt_br translations
* Update ja_JP translations

[1.14.0]
* Update base image to v3
* Update PHP to 7.4

[1.14.1]
* Update Kanboard to 1.2.19
* Trim user agent for RememberMe sessions because MySQL use a varchar(255) column
* Fixed createLdapUser API procedure when LDAP groups are not configured
* Write RememberMe cookie only after the two-factor code has been validated
* Avoid warning when removing a plugin zip archive
* Add new hook model:task:duplication:aftersave
* Bump symfony/stopwatch from 5.2.3 to 5.2.4
* Bump pimple/pimple from 3.3.1 to 3.4.0
* Bump gregwar/captcha from 1.1.8 to 1.1.9
* Added new analytic component: "Estimated vs actual time per column"
* Do not retain any changes between shared plugins variables
* Display number of tasks according to filter
* Add support for LDAP protocol/host/port configuration by URL; make BASE_DN optional
* Use an absolute file path in AssetHelper class for css() & js() functions
* Add IP address to authentication error logs
* Add interpolation expressions to e-mail subject in automatic action "Send a task by email to someone"
* Add Hungarian Forint to the list of currencies

[1.14.2]
* Update Kanboard to 1.2.20
* Duplicate tags when moving or duplicating tasks to another project
* Bump symfony/stopwatch to 5.3.0
* Avoid user enumeration by using avatar image URL
* Invalidate captcha after it is used
* Avoid user enumeration using password reset functionality
* Add missing CSRF checks
* Fix bug in search when using the plus sign
* Close dialogs using Escape key even if focus is in input field
* Add a min="0" attribute to task_list form input
* Keep swimlane headers at the top
* Catch error when trying to upload empty or invalid avatar image
* Added new template hooks
* Update translations

[1.14.3]
* Update Kanboard to 1.2.21
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.21)
* Fix and update Composer autoload
* Add plugin hook for document attachments
* Improve board column header alignment
* Ignore project_id for file attachments download URL (already checked elsewhere)
* Update translations
* Clarify meaning of LDAP_USER_CREATION in config.default.php
* Fix wrong internal link when converting a subtask to task (MySQL only)
* Use the overridable Markdown parser for previews
* Update call_user_func_array() calls to be compatible with PHP 8
* Enable external group synchronization deactivation
* Fix tooltip shifting on long descriptions
* Add position argument to API procedure updateSubtask()
* Bump Docker image to Alpine 3.15.0
* Bump symfony/stopwatch to 5.4.0
* Bump pimple/pimple to 3.5.0

[1.14.4]
* Better apache configs

[1.14.5]
* Update Kanboard to 1.2.22
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.22)
* Remove project_id from task URLs
* Update da_DK translations
* Add automatic action to set the due date when the task is moved away from a specific column
* Condense wording on inferred action and update translations
* Add EVENT_CREATE and EVENT_CREATE_UPDATE events to TaskMoveColumnCategoryChange action

[1.14.6]
* Update Kanboard to 1.2.23
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.23)
* Open SVG, Ogg, and some video file attachments in browser
* Added more video, music, code and spreadsheet extensions to show better file attachment icons
* Updated jQuery to latest stable version
* Updated Docker image to PHP 8.1 and Alpine Linux 3.16
* Renamed default branch from master to main
* Bumped phpunit/phpunit from 9.5.14 to 9.5.23
* Bumped symfony/finder from 5.4.3 to 5.4.11
* Fixed subtask translation when using different languages
* Added Project Overview document template hook
* Updated translations
* Fixed wrong foreign key constraint on table subtask_time_tracking table. The constraints references a no-longer-existing table task_has_subtasks
* Fixed regression regarding subtask reordering

[1.14.7]
* Update Kanboard to 1.2.24
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.24)
* Fixed deprecation warnings when a project or a task description is null
* Fixed missing condition in TaskAssignDueDateOnMoveColumn action
* Fixed Reopening of dropdown menus
* Fixed internal link creation on subtask to task conversion if language is not English
* Use a HMAC to sign and validate CSRF tokens, instead of generating random ones and storing them in the session data
* Set explicitly the time picker control to select instead of slider

[1.14.8]
* Update Kanboard to 1.2.25
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.25)
* Update links to the new documentation website
* Update German translation

[1.15.0]
* Update base image to 4.0.0

[1.15.1]
* Update Kanboard to 1.2.26
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.26)
* Fire events after TaskMoveColumnOnDueDate action
* Update date parsing logic to be compatible with PHP 8.2
* Fix potential XSS on the Settings / API page
* Use wildcard operator for tag filter
* Fix broken user mentions in popup comment form

[1.15.2]
* Update Kanboard to 1.2.27
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.27)
* Fix category filter when the category name is a number
* Better handling of max file upload size according to PHP settings
* Add dropdown menu on the board to reorder tasks by ID
* Separate font-family specification for input and textarea. This avoids the use of !important in custom CSS
* Change the total number of tasks displayed in the table header to match the description "Total number of tasks in this column across all swimlanes"
* Allow full name to be retrieved by the reverse proxy authentication
* Fix pull-right CSS class alignment
* Use a separate dropdown menu for column sorting

[1.15.3]
* Update Kanboard to 1.2.28
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.28)
* Trigger `EVENT_MOVE_COLUMN` event when moving task to another swimlane
* Allow moving closed tasks when using the API
* Duplicate external links when duplicating tasks
* Add support for comparison operator to priority filter
* Prevents users to convert subtaks to tasks when custom role does not allow it
* Avoid deprecation messages when sending an email with PHP 8.2
* Declare most common routes to have nice URLs
* Improve wording of bulk action modal to move tasks position
* Allow closing modals by clicking on the background

[1.15.4]
* Update Kanboard to 1.2.29
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.29)
* Avoid potential clipboard based cross-site scripting (CVE-2023-32685)
* Add themes support: dark, light and automatic mode
* Fix broken "Hide this Column" feature
* Do not close modals when clicking on the background if the form has changed
* Fix incorrect route for "My Activity Stream"
* Fix incorrect parameter encoding when using URLs rewriting
* Add support for task links in Markdown headings
* Handle 413 responses from Nginx when uploading files too large

[1.15.5]
* Update Kanboard to 1.2.30
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.30)
* CVE-2023-33956: Parameter based Indirect Object Referencing leading to private file exposure
* CVE-2023-33968: Missing access control allows user to move and duplicate tasks to any project in the software
* CVE-2023-33969: Stored XSS in the Task External Link Functionality
* CVE-2023-33970: Missing access control in internal task links feature
* Avoid PHP warning caused by session_regenerate_id()
* Avoid CSS issue when upgrading to v1.2.29 without flushing user sessions

[1.15.6]
* Update Kanboard to 1.2.32
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.32)
* Fix unexpected EventDispatcher exception in cronjob and during logout
* Integration Tests: Run apt update before installing Apache
* Automatic action TaskMoveColumnClosed does not log column movement
* Tweak Sqlite connection settings to reduce database locked errors
* Bump phpunit/phpunit from 9.6.9 to 9.6.10

[1.15.7]
* Update Kanboard to 1.2.33
* Update base image to 4.2.0
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.33)
* Do not close modals when clicking on the background
* Add Bulgarian translation
* Update Ukrainian and Russian translations
* Show the two factor form in the middle of the screen like the login form does
* Do not override the creator_id with the current logged user if the task is imported
* Add basic Dev Container configs
* Add adaptive SVG favicon and more SVG variants:
* Remove project_id from task links (A few were missed in #4892)
* Remove unused and invalid method in ProjectModel
* Update phpunit/phpunit and symfony/* dependencies
* Update vendor folder

[1.15.8]
* Update Kanboard to 1.2.34
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.34)
* API: Avoid PHP notice when searching for a project name that does not exist
* Update Bulgarian translation
* Bump symfony/console from 5.4.28 to 5.4.32
* Bump phpunit/phpunit from 9.6.13 to 9.6.15

[1.15.9]
* Update Kanboard to 1.2.35
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.35)
* Add missing HTML escaping when showing group membership in user profile (CVE-2024-22720)
* Update Dutch translation
* Update Bulgarian translation

[1.15.10]
* Update Kanboard to 1.2.36
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.36)
* Add comments visibility
* Add explicit int casting to avoid PHP 8 TypeError when having empty automatic action parameters
* Add new config option `DASHBOARD_MAX_PROJECTS`
* Add reply feature to comments
* Fix search bar layout when adding more buttons via third-party plugins
* Introduce a Git hook to automatically update version.txt during Git checkout

[1.15.11]
* Update Kanboard to 1.2.37
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.37)
* Add comments visibility
* Add explicit int casting to avoid PHP 8 TypeError when having empty automatic action parameters
* Add new config option `DASHBOARD_MAX_PROJECTS`
* Add reply feature to comments
* Fix search bar layout when adding more buttons via third-party plugins

[1.16.0]
* Migrate to OIDC

[1.16.1]
* Update Kanboard to 1.2.38
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.38)

[1.16.2]
* Update Kanboard to 1.2.39
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.39)
* fix: remove CSS which caused responsive issues on mobile
* fix: incorrect template condition that set the username field to read only for remote users
* fix: tasks count across swimlanes was incorrect
* fix: avoid warning from libpng when loading PNG image with incorrect iCCP profiles
* feat: improve column header task counts

[1.16.3]
* Update Kanboard to 1.2.40
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.40)
* fix: avoid PHP error if no subtask in progress is found
* fix: avoid potential XSS and HTML injection in comment replies
* fix: prevent duplicated columns when enabling per-swimlane column task limits
* fix(api): check comment visibility in API procedures
* fix(api): verify comment ownership in API procedures
* fix(mssql): escape identifiers in timesheet queries
* fix(mssql): use ANSI OFFSET/FETCH syntax for pagination queries
* fix(test): use explicit ORDER BY for queries returning multiple rows

[1.16.4]
* Update Kanboard to 1.2.41
* [Full changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.41)
* feat: add new plugin hooks in project forms
* feat: add option to add BOM at the beginning of CSV files (required for Microsoft Excel)
* feat: validate app config form values
* feat: add cancel button on 2FA code validation screen
[1.16.5]
* Update kanboard to 1.2.42
* [Full Changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.42)
* fix: validate translation filename before loading locales
* fix: avoid path traversal in `FileStorage`
* feat: add Peruvian Sol to the list of currencies
* build(deps): bump `symfony/finder` from `5.4.43` to `5.4.45`
* build(deps-dev): bump `symfony/stopwatch` from `5.4.40` to `5.4.45`

[1.16.6]
* checklist added to CloudronManifest

[1.16.7]
* Update kanboard to 1.2.43
* [Full Changelog](https://github.com/kanboard/kanboard/releases/tag/v1.2.43)
* fix: verify the session hasn't expired before returning data
* fix: avoid PHP 8.4 deprecation notices in third-party libraries
* fix: avoid Composer warnings regarding PSR compatibility
* feat(locale): add missing Brazilian Portuguese translations

