# Kanboard Cloudron App

This repository contains the Cloudron app package source for [Kanboard](http://kanboard.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=net.kanboard.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id net.kanboard.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd kanboard-app

cloudron build
cloudron install
```

## Debugging

Enable `DEBUG` to true in config.php.template

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if things are still ok. 

```
cd kanboard-app/test

npm install
EMAIL=<email> USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

