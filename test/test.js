#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();

        await browser.sleep(5000); // this is a hack to let kanboard finish setting up the db (which is one first http request?)
        // should get redirected to '/login'

        await browser.get('https://' + app.fqdn);
        await waitForElement(By.id('form-username'));
        await browser.findElement(By.id('form-username')).sendKeys(username);
        await browser.findElement(By.id('form-password')).sendKeys(password);
        await browser.findElement(By.tagName('form')).submit();

        await browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                return url === 'https://' + app.fqdn + '/'; // there is a trailing last even if the browser bar doesn't show it!
            });
        }, TEST_TIMEOUT);
    }

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function loginOIDC(username, password) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);
        await browser.sleep(2000);

        await waitForElement(By.xpath('//a[contains(., "OAuth2 login")]'));
        await browser.findElement(By.xpath('//a[contains(., "OAuth2 login")]')).click();
        await browser.sleep(2000);

        await waitForElement(By.id('inputUsername'));
        await browser.findElement(By.id('inputUsername')).sendKeys(username);
        await browser.findElement(By.id('inputPassword')).sendKeys(password);
        await browser.findElement(By.id('loginSubmitButton')).click();

        await browser.sleep(3000);
        await waitForElement(By.xpath('//h1/span[@class="title" and contains(., "Dashboard")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//div[@class="avatar-letter"]/ancestor::a'));
        await browser.findElement(By.xpath('//div[@class="avatar-letter"]/ancestor::a')).click();
        await browser.sleep(3000);
        const logoutButton = (await browser.findElements(By.xpath('//a[contains(@href, "logout")]'))).pop(); // last
        await browser.wait(until.elementIsVisible(logoutButton), TEST_TIMEOUT);
        await logoutButton.click();
        await waitForElement(By.id('form-username'));
    }

    async function locateExistingBoard() {
        browser.get(`https://${app.fqdn}/board/1`);
        await waitForElement(By.xpath('//*[contains(text(), "Backlog")]'));
        await waitForElement(By.xpath('//*[contains(text(), "Ready")]'));
        await waitForElement(By.xpath('//*[contains(text(), "Work in progress")]'));
        await waitForElement(By.xpath('//*[contains(text(), "Done")]'));
    }

    async function createProject() {
        await browser.get('https://' + app.fqdn + '/project/create');
        await browser.findElement(By.id('form-name')).sendKeys('cloudron');
        await browser.findElement(By.tagName('form')).submit();
        await browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                console.log(url);
                return url === 'https://' + app.fqdn + '/project/1';
            });
        }, 40000);
    }

    async function checkApplicationUrl() {
        await browser.get('https://' + app.fqdn + '/settings/application');
        await waitForElement(By.id('form-application_url'));

        const url = await browser.findElement(By.id('form-application_url')).getAttribute('value');
        expect(url).to.be('https://' + app.fqdn + '/');
    }

    async function hasAdminRole() {
        await browser.get(`https://${app.fqdn}/user/show/1`);
        await waitForElement(By.xpath('//strong[text() = "Administrator"]'));
    }

    async function installExtension() {
        await browser.get(`https://${app.fqdn}/extensions/directory`);
        const installXPath = '//tr[./th/a[contains(text(), "Calendar")]]/following-sibling::tr[1]//a[contains(text(), "Install")]';
        const button = await browser.findElement(By.xpath(installXPath));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.findElement(By.xpath(installXPath)).click();
        await browser.sleep(4000);
    }

    async function checkExtension() {
        await browser.get(`https://${app.fqdn}/extensions`);
        await waitForElement(By.xpath('//a[contains(@href, "pluginId=Calendar")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no sso
    it('install app (NO SSO)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login as admin', login.bind(null, 'admin', 'admin'));
    it('set up the application url correctly', checkApplicationUrl);
    it('has admin role', hasAdminRole);
    it('can create a new project', createProject);
    it('can install extension', installExtension);
    it('can check extension', checkExtension);
    it('can logout', logout);

    it('uninstall app (NO SSO)', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // sso
    it('clear cache', clearCache);
    it('install app (SSO)', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login as admin', login.bind(null, 'admin', 'admin'));
    it('set up the application url correctly', checkApplicationUrl);
    it('has admin role', hasAdminRole);
    it('can create a new project', createProject);
    it('can install extension', installExtension);
    it('can check extension', checkExtension);
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id, EXEC_ARGS); });

    it('clear cache', clearCache);
    it('can login as admin', login.bind(null, 'admin', 'admin'));
    it('can locate existing project board', locateExistingBoard);
    it('set up the application url correctly', checkApplicationUrl);
    it('can check extension', checkExtension);
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('clear cache', clearCache);
    it('can login as admin', login.bind(null, 'admin', 'admin'));
    it('can locate existing project board', locateExistingBoard);
    it('can check extension', checkExtension);
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });
    it('clear cache', clearCache);
    it('can login as admin', login.bind(null, 'admin', 'admin'));
    it('set up the application url correctly', checkApplicationUrl);
    it('can check extension', checkExtension);
    it('can locate existing project board', locateExistingBoard);
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('clear cache', clearCache);
    it('can install old app', function () { execSync('cloudron install --appstore-id net.kanboard.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, 'admin', 'admin'));
    it('set up the application url correctly', checkApplicationUrl);
    it('has admin role', hasAdminRole);
    it('can create a new project', createProject);
    it('can install extension', installExtension);
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });

    it('clear cache', clearCache);
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, 'admin', 'admin'));
    it('can locate existing project board', locateExistingBoard);
    it('has admin role', hasAdminRole);
    it('set up the application url correctly', checkApplicationUrl);
    it('can check extension', checkExtension);
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
