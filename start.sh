#!/bin/bash

set -eu 

mkdir -p /app/data/plugins /app/data/data /run/kanboard/sessions

echo -e "[client]\npassword=${CLOUDRON_MYSQL_PASSWORD}" > /run/kanboard/mysql-extra
readonly mysql="mysql --defaults-file=/run/kanboard/mysql-extra --user=${CLOUDRON_MYSQL_USERNAME} --host=${CLOUDRON_MYSQL_HOST} -P ${CLOUDRON_MYSQL_PORT} ${CLOUDRON_MYSQL_DATABASE}"

setup_application_url() {
    set -eu

    if $mysql -e "REPLACE INTO settings (\`option\`, \`value\`) VALUES (\"application_url\", \"${CLOUDRON_APP_ORIGIN}/\")" 2>/dev/null; then
        echo "==> Application URL updated"
    else
        echo "==> Failed to set application url"
    fi
}

setup_oidc() {
    echo "==> Ensure OIDC settings"

    cp -rf /app/code/plugins.orig/OAuth2 /app/data/plugins

    # LDAP should be removed on the next release
    ldapUsers=$($mysql -NB -e "SELECT count(*) FROM users WHERE is_ldap_user=1 AND oauth2_user_id IS NULL" 2>/dev/null)
    if [[ $ldapUsers -gt 0 ]]; then
        echo "==> Migrate LDAP users to OIDC"
        echo "====> ${ldapUsers} to be migrated"
        $mysql -NB -e "UPDATE users SET oauth2_user_id=username, is_ldap_user=NULL WHERE is_ldap_user=1 AND oauth2_user_id IS NULL" 2>/dev/null
    fi

    # CLOUDRON_OIDC_PROVIDER_NAME is not supported
    OIDC_SETTINGS=$(cat <<EOT
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_account_creation", "1");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_authorize_url", "${CLOUDRON_OIDC_AUTH_ENDPOINT}");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_client_id", "${CLOUDRON_OIDC_CLIENT_ID}");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_client_secret", "${CLOUDRON_OIDC_CLIENT_SECRET}");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_email_domains", "");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_key_email", "email");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_key_group_filter", "");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_key_groups", "");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_key_name", "name");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_key_user_id", "sub");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_key_username", "preferred_username");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_scopes", "openid profile email");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_token_url", "${CLOUDRON_OIDC_TOKEN_ENDPOINT}");
REPLACE INTO settings (\`option\`, \`value\`) VALUES ("oauth2_user_api_url", "${CLOUDRON_OIDC_PROFILE_ENDPOINT}");
EOT
    )
    echo ${OIDC_SETTINGS} | $mysql 2>/dev/null
}

sed -e "s/##MAIL_FROM/${CLOUDRON_MAIL_FROM}/" \
    -e "s/##MAIL_SMTP_SERVER/${CLOUDRON_MAIL_SMTP_SERVER}/" \
    -e "s/##MAIL_SMTP_PORT/${CLOUDRON_MAIL_SMTP_PORT}/" \
    -e "s/##MAIL_SMTP_USERNAME/${CLOUDRON_MAIL_SMTP_USERNAME}/" \
    -e "s/##MAIL_SMTP_PASSWORD/${CLOUDRON_MAIL_SMTP_PASSWORD}/" \
    -e "s/##MYSQL_USERNAME/${CLOUDRON_MYSQL_USERNAME}/" \
    -e "s/##MYSQL_PASSWORD/${CLOUDRON_MYSQL_PASSWORD}/" \
    -e "s/##MYSQL_HOST/${CLOUDRON_MYSQL_HOST}/" \
    -e "s/##MYSQL_PORT/${CLOUDRON_MYSQL_PORT}/" \
    -e "s/##MYSQL_DATABASE/${CLOUDRON_MYSQL_DATABASE}/" \
    /app/pkg/config.php.template > /run/kanboard/config.php


if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

if [[ ! -f /app/data/customconfig.php ]]; then
    echo "==> Copying customconfig.php.template"
    cp /app/pkg/customconfig.php.template /app/data/customconfig.php
fi

table_count=$($mysql -NB -e "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '${CLOUDRON_MYSQL_DATABASE}';" 2>/dev/null)

if [[ "${table_count}" == "0" ]]; then
    echo "==> Initializing database"
    # this is only the seed file, must always run migration afterwards
    cat /app/code/app/Schema/Sql/mysql.sql | $mysql 2>/dev/null
fi

echo "==> Migrating database"
sudo -u www-data php /app/code/cli db:migrate

setup_application_url

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    setup_oidc
fi

chown -R www-data:www-data /app/data /run/kanboard

echo "==> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
