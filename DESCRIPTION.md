Kanboard is a project management software that use the Kanban methodology.

### Features
* Fast and simple to use
* Visualize your work
* Limit your work in progress to be more efficient
* Customize your boards according to your business activities
* Multiple boards with the ability to drag and drop tasks
* Reports and analytics
* Access from anywhere with a modern browser
* Visual and clear overview of your tasks
* Search and filter tasks
* Single dashboard for all projects
* Tasks, subtasks, attachments and comments
* Swimlines, Gantt charts and much more

### About Kanban

Kanban is a methodology originally developed by Toyota to be more efficient.

There is only two constraints imposed by Kanban:

  * Visualize your workflow
  * Limit your work in progress
  * Measure performance and improvement
